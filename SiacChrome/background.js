// Quando estiver instalando/atualizando:
chrome.runtime.onInstalled.addListener(function () {
    // Remover as regras de mudança de página:
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        // Para adicionar a regra abaixo:
        chrome.declarativeContent.onPageChanged.addRules([{
                // Se:
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        // A url da página conter:
                        pageUrl: {urlContains: 'convenios.gov.br'},
                    })],
                // Nesse caso, mostramos a Page Action da extensão:
                actions: [new chrome.declarativeContent.ShowPageAction()]
            }]);
    });

    //função chamada ao clicar no link
    chrome.browserAction.onClicked.addListener(function (tab) {
		alert('Você será redirecionado para o site de convênios');
        window.open("https://idp.convenios.gov.br/idp/", '_blank');
    });
});
