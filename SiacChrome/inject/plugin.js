var SiacPlugin = {
    config: {
        URL_API: 'https://siac.sudam.gov.br/api/',
        URL_LOGIN_SIAC: localStorage.getItem('URL_LOGIN_SIAC'),
        CONSULTA_COMPLETA: localStorage.getItem('CONSULTA_COMPLETA'),
        MANTER_MOVIMENTACAO: localStorage.getItem('MANTER_MOVIMENTACAO'),
        CONSULTA_PRESTACAO_CONTAS: localStorage.getItem('CONSULTA_PRESTACAO_CONTAS'),
        CONSULTA_FISCAL_ACOMPANHANTE: localStorage.getItem('CONSULTA_FISCAL_ACOMPANHANTE'),
        CONSULTA_SITUACAO_LICITACAO: localStorage.getItem('CONSULTA_SITUACAO_LICITACAO'),
        CONSULTA_SITUACAO_PBTR: localStorage.getItem('CONSULTA_SITUACAO_PBTR'),
    },
    conveniosCarregar: function (callback) {

        /**
         * Busca os convênios a serem atualizados e armazena no localStorage
         */

        //mantem localStorage ja existente
        if (window.location.hash.slice(1) == "materLocalStorage") {
            SiacPlugin.proximoConvenioConsultar();

            return;
        }


        //limpa o local storage
        localStorage.clear();

        $.get(SiacPlugin.config.URL_API + 'plugin-convenios.php', function (data) {
            if (localStorage && !localStorage.getItem('size')) {
                try {
                    for (var i in data.data.convenios) {
                        localStorage.setItem(i, JSON.stringify({
                            NR_CONVENIO: data.data.convenios[i].NR_CONVENIO,
                            ID_PROPOSTA: data.data.convenios[i].ID_PROPOSTA,
                            VL_DATA_ATUALIZACAO: data.data.convenios[i].VL_DATA_ATUALIZACAO
                        }));
                    }

                    SiacPlugin.config = data.data.config;
                    localStorage.setItem('URL_LOGIN_SIAC', SiacPlugin.config.URL_LOGIN_SIAC);
                    localStorage.setItem('CONSULTA_COMPLETA', SiacPlugin.config.CONSULTA_COMPLETA);
                    localStorage.setItem('MANTER_MOVIMENTACAO', SiacPlugin.config.MANTER_MOVIMENTACAO);
                    localStorage.setItem('CONSULTA_PRESTACAO_CONTAS', SiacPlugin.config.CONSULTA_PRESTACAO_CONTAS);
                    localStorage.setItem('CONSULTA_FISCAL_ACOMPANHANTE', SiacPlugin.config.CONSULTA_FISCAL_ACOMPANHANTE);
                    localStorage.setItem('CONSULTA_SITUACAO_LICITACAO', SiacPlugin.config.CONSULTA_SITUACAO_LICITACAO);
                    localStorage.setItem('CONSULTA_SITUACAO_PBTR', SiacPlugin.config.CONSULTA_SITUACAO_PBTR);

                    if (callback) {
                        callback(data.data);
                    }
                } catch (e) {
                    console.error('Erro de local storage');
                }
            }

        }, 'json');
    },
    convenioAtual: function () {

        /**
         * Retorna o convênio que esta sento consultado
         */

        var indexConvenio = localStorage.getItem('indexConvenio');
        if (!indexConvenio) {
            indexConvenio = 0;
            localStorage.setItem('indexConvenio', indexConvenio);
        }
        var convenio = localStorage.getItem(indexConvenio);
        if (convenio) {
            return JSON.parse(convenio);
        } else {
            return null;
        }
    },
    proximoConvenioConsultar: function () {

        /**
         * Retorna o próximo convênio a ser consultado
         */

        var indexConvenio = localStorage.getItem('indexConvenio');

        if (indexConvenio === null) {
            indexConvenio = 0;

            var convenio = localStorage.getItem(0);
        } else {
            var convenio = localStorage.getItem(indexConvenio);
        }

        if (convenio) {
            convenio = JSON.parse(convenio);
        } else {
            alert('Convênios atualizados, você será redirecionado par ao SIAC');

            window.location.href = SiacPlugin.config.URL_LOGIN_SIAC;

            return false;
        }

        //atualiza a data do convenio no localStorage
        convenio.VL_DATA_ATUALIZACAO = SiacPlugin.dataAtual();

        localStorage.setItem(indexConvenio, JSON.stringify(convenio));

        if (localStorage.getItem('indexConvenio') === null) {
            execPost(convenio.NR_CONVENIO);
        } else {
            //coloca a data de atualização do convenio como a data atual
            SiacPlugin.conveioDataAtualizarPlugin(convenio, function () {

                //depois de atualizar a data do convenio atual pula para o proximo convenio
                indexConvenio = parseInt(indexConvenio) + 1;

                localStorage.setItem('indexConvenio', indexConvenio);

                var convenio = SiacPlugin.convenioAtual();

                execPost(convenio.NR_CONVENIO);
            });
        }

        function execPost(nrConvenio) {
            //faz o post para verificar o convênio
            $.post(SiacPlugin.config.CONSULTA_COMPLETA, {numeroConvenio: nrConvenio}, function (resource) {
                //verifica se existe convênio
                if ($(resource).find('.numeroConvenio a').length) {
                    //redireciona para a tela do convênio
                    window.location.href = 'https://voluntarias.plataformamaisbrasil.gov.br' + $(resource).find('.numeroConvenio a').attr('href');
                } else {
                    //se for o primeiro convenio adiciona 1 para nao ficar em loop
                    if (localStorage.getItem('indexConvenio') === null) {
                        localStorage.setItem('indexConvenio', 1);
                    }
                    //se não existir, consuta o próximo convênio
                    SiacPlugin.proximoConvenioConsultar();
                }
            });
        }

    },
    clausulaSuspensiva: function () {

        /**
         * Método que verifica se existe clausula suspensiva e envia para a base de dados
         */

            //pega os dados da cláusula suspensiva
        var dataPrevista = document.getElementById('tr-voltarDataParaRetirada');
        var motivoClausula = document.getElementById('tr-voltarMotivo');

        if (dataPrevista || motivoClausula) {

            if (dataPrevista) {
                dataPrevista = dataPrevista.lastElementChild.innerHTML.replace(/\s/g, '');
            } else {
                dataPrevista = null;
            }

            if (motivoClausula) {
                motivoClausula = motivoClausula.lastElementChild.innerHTML.replace(/\s/g, '');
            } else {
                motivoClausula = null;
            }

            //recupera o convenio que esta sendo atualizado
            var convenio = SiacPlugin.convenioAtual();

            var conveioPlugin = {
                NR_CONVENIO: convenio.NR_CONVENIO,
                VL_DATA_RESOLUCAO_CLAUSULA_SUSPENSIVA: dataPrevista,
                VL_MOTIVO_CLAUSULA_SUSPENSIVA: motivoClausula
            };

            SiacPlugin.cadastraConveioPlugin(conveioPlugin, function (resource) {
                //se não existir enviar para a tela das movimentações
                window.location.href = SiacPlugin.config.MANTER_MOVIMENTACAO;
            });

        } else {
            //NOTA - Colocar o redirecionamento para a tela do próximo dado
        }
    },
    movimentacaoFinanceira: function () {

        /**
         * Método que verifica se existe movimentação financeira e envia para a base de dados
         */

        $.get(SiacPlugin.config.MANTER_MOVIMENTACAO, function (resource) {

            //recupera o convenio que esta sendo atualizado
            var convenio = SiacPlugin.convenioAtual();

            var movimentacoes = [];
            $('#formConsultaMovimentacaoFinanceira\\:dtTableBeans').find('.dr-table-firstrow').each(function (a, b) {
                movimentacoes.push({
                    NR_CONVENIO: convenio.NR_CONVENIO,
                    NUMERO: $($(b).find('td')[0]).find('span').html(),
                    DATA: $($(b).find('td')[1]).find('span').html(),
                    VALOR_LIQUIDO: $($(b).find('td')[4]).find('span').html(),
                    TIPO: $($(b).find('td')[6]).find('span').html()
                });
            });

            SiacPlugin.cadastraMovimentacaoPlugin(movimentacoes, function (resource) {
                SiacPlugin.consultaSituacaoLicitacao();
            });
        });
    },
    consultaPrestacaoContas: function () {

        /**
         * Método que verifica se existe prestacao de contas e envia para a base de dados
         */

            //recupera convenio atual
        var convenio = SiacPlugin.convenioAtual();

        var tbResultado = $("#formResultadoConsultaConvenios\\:dtResultadoConsultaConvenios");

        //verifica se existe tabela de resultado
        if (tbResultado.length > 0) {
            var linhaResultado = $("tbody tr", tbResultado);

            //verifica se existe alguma linha na tabela de resultado
            if (linhaResultado.length > 0) {
                var link = $($("tbody tr:first-child td a", tbResultado)[1]);

                //adiciona a situacao da prestacao de contas ao convenio atual
                convenio.prestacaoContas = link.html();

                //salva o convenio atual com a prestacao de constas adicionado ao mesmo anteriormente
                localStorage.setItem(localStorage.getItem("indexConvenio"), JSON.stringify(convenio));

                var form = document.forms["formResultadoConsultaConvenios"];

                if (typeof form.elements["formResultadoConsultaConvenios:_idcl"] == 'undefined') {
                    var newInput = document.createElement('input');
                    newInput.setAttribute('type', 'hidden');
                    newInput.setAttribute('id', 'formResultadoConsultaConvenios:_idcl');
                    newInput.setAttribute('name', 'formResultadoConsultaConvenios:_idcl');
                    newInput.setAttribute('value', 'formResultadoConsultaConvenios:dtResultadoConsultaConvenios:0:_idJsp45');
                    form.appendChild(newInput);
                }
                else {
                    form.elements['formResultadoConsultaConvenios:_idcl'].value = 'formResultadoConsultaConvenios:dtResultadoConsultaConvenios:0:_idJsp45';
                }


                if (document.forms["formResultadoConsultaConvenios"].onsubmit) {
                    var result = document.forms["formResultadoConsultaConvenios"].onsubmit();
                    if ((typeof result == 'undefined') || result) {
                        document.forms["formResultadoConsultaConvenios"].submit();
                    }

                }
                else {
                    document.forms["formResultadoConsultaConvenios"].submit();
                }

            } else {
                SiacPlugin.consultaFiscalAcompanhante();
            }

        } else {
            var input = $("#formConsultaConvenio\\:inputTextNumeroConvenio");

            if (input.length > 0) {

                input.val(convenio.NR_CONVENIO);

                jQuery("#formConsultaConvenio\\:buttonConsultar").trigger('click');
            }
        }


    },
    dataPrestacaoContas: function () {
        var tabelas = $("table", "#mainForm");

        var convenio = SiacPlugin.convenioAtual();

        //verifica se existe a 3 tabela de resultados que é a que representa a prestacao de contas
        if (tabelas.length >= 3) {
            var dados = {
                vigencia: $("td .info", tabelas[2])[0].innerHTML.trim().split(" ")[1].trim(),
                limite: $("td .info", tabelas[2])[1].innerHTML.trim().split(" ")[1].trim(),
                nrConvenio: convenio.NR_CONVENIO,
                prestacaoContas: convenio.prestacaoContas
            };

            SiacPlugin.cadastraPrestacaoContas(dados, function () {
                SiacPlugin.consultaFiscalAcompanhante();
            });
        } else {
            SiacPlugin.consultaFiscalAcompanhante();
        }
    },
    consultaFiscalAcompanhante: function () {
        $.get(SiacPlugin.config.CONSULTA_FISCAL_ACOMPANHANTE, function (html) {

            var fiscaisVinculados = jQuery("#row tbody tr", html);

            if (fiscaisVinculados.length > 0) {

                //recupera o convenio que esta sendo atualizado
                var convenio = SiacPlugin.convenioAtual();

                var fiscais =[];

                jQuery.each(fiscaisVinculados, function (k, tr) {
                    var nome = $("td:nth(0)", tr).text().trim();
                    var cpf = $("td:nth(1)", tr).text().trim();
                    var dataDesvinculo = $("td:nth(6)", tr).text().trim();

                    if(dataDesvinculo == ""){
                        fiscais.push({ nome: nome });
                    }
                });

                $.post(SiacPlugin.config.URL_API + "plugin-convenios.php?acao=cadastraFiscais", { NR_CONVENIO: convenio.NR_CONVENIO, FISCAIS: fiscais }, function () {
                    SiacPlugin.redirecionaQuadroResumo();
                });

            } else {
                SiacPlugin.redirecionaQuadroResumo();
            }

        });
    },
    consultaSituacaoLicitacao: function(){

        //recupera o convenio que esta sendo atualizado
        var convenio = SiacPlugin.convenioAtual();

        var params = { NR_CONVENIO: convenio.NR_CONVENIO, SITUACAO_LICITACAO: null, DATA_UPLOAD: null };

        $.get(SiacPlugin.config.CONSULTA_SITUACAO_LICITACAO, function(pageLicitacao){
            var sit = $("#row tbody tr:first-child td:nth(4)", pageLicitacao);

            if(sit != undefined){
                params.SITUACAO_LICITACAO = sit.text().trim();

                var urlUpload = $("#row tbody tr:first-child td:last-child a", pageLicitacao).attr('href');

                if(urlUpload != undefined){
                    //realiza requisicao sincronizada
                    $.ajax({
                        type: 'GET',
                        url: urlUpload.split("='")[1].replace("';", ""),
                        async: false,
                        success: function(page){
                            params.DATA_UPLOAD = $("#row tbody tr:first-child td:nth(1)", page).text().trim();
                        }
                    });
                }

                //realiza requisicao sincronizada
                $.ajax({
                    url: SiacPlugin.config.URL_API + "plugin-convenios.php?acao=cadastraSituacaoLicitacao",
                    async: false,
                    data: params,
                    type: 'POST'
                });

            }

            window.location.href = SiacPlugin.config.CONSULTA_PRESTACAO_CONTAS;

        });
    },
    redirecionaQuadroResumo: function(){

        var convenio = SiacPlugin.convenioAtual();

        if (convenio.quadro_resumo_link != undefined) {
            window.location.href = convenio.quadro_resumo_link;
        } else {
            $.get(SiacPlugin.config.CONSULTA_SITUACAO_PBTR, function (r) {
                var SituacaoPBTR = $("[name='listarDocumentosProjetoBasicoListandoDocumentosDoProjetoBasicoINCLUIRForm'] table tr:eq(1) td:last-child", r ).text().trim();

                SiacPlugin.cadastraSituacaoPbTr({ SITUACAO_PBTR : SituacaoPBTR, NR_CONVENIO : convenio.NR_CONVENIO }, function(){
                    SiacPlugin.proximoConvenioConsultar();
                });
            });

        }
    },
    indexQuadroResumo: function () {

        /**
         * Método que verifica se existe quadro de resumo
         */

        $.post("https://mandatarias.plataformamaisbrasil.gov.br/projeto-basico/private/index.jsf", {
            "javax.faces.partial.ajax": "true",
            "javax.faces.source": "tabViewMandatarias",
            "javax.faces.partial.execute": "@none tabViewMandatarias",
            "javax.faces.partial.render": "tabViewMandatarias",
            "javax.faces.behavior.event": "tabChange",
            "javax.faces.partial.event": "tabChange",
            "tabViewMandatarias_contentLoad": "true",
            "tabViewMandatarias_newTab": "tabViewMandatarias:tabQuadroResumo",
            "tabViewMandatarias_tabindex": "7",
            "javax.faces.ViewState": $('#javax\\.faces\\.ViewState').val()
        }, function (r) {

            var xmlMandatarias = $.parseXML($(r).find("#tabViewMandatarias").text());

            var QuadroResumo = [];

            var tbQuadroResumo = jQuery(xmlMandatarias).find("table").last();

            //percorre as linhas da tabela de quadro de resumo
            $.each($("tbody tr", tbQuadroResumo), function () {
                var infos = $("td", this);

                QuadroResumo.push({
                    DATA_HORA: $(infos[0]).text(),
                    EVENTO: $(infos[1]).text(),
                    RESPONSAVEL: $(infos[2]).text(),
                    CONSIDERACAO: $(infos[3]).text(),
                    SITUACAO: $(infos[4]).text()
                });
            });

            var labelNrConv = $("#tabViewMandatarias\\:formDadosBasicos\\:j_idt43");

            var divNrConv = labelNrConv.parent();

            labelNrConv.remove();

            SiacPlugin.cadastraQuadroResumo({ quadroResumo: QuadroResumo, NR_CONVENIO : divNrConv.text().trim().split("/")[0], SITUACAO_PBTR: $("#tabViewMandatarias\\:formDadosBasicos\\:situacaoDocOrc").text() }, function () {
                // SiacPlugin.proximoConvenioConsultar();
                window.location.href = 'https://voluntarias.plataformamaisbrasil.gov.br/voluntarias/Principal/Principal.do#materLocalStorage';
            });

        });
    },
    conveiosStatusTela: function () {

        /**
         * Método de montagem de tela para o usuário ver o status do convênio
         */

            //recupera os convenios do local storage
        var convenios = [];
        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            var value = localStorage.getItem(key);
            if ($.isNumeric(key)) {
                convenios.push(JSON.parse(value));
            }
        }

        //ordena pela data
        convenios.sort(function (a, b) {
            return new Date(b.VL_DATA_ATUALIZACAO) - new Date(a.VL_DATA_ATUALIZACAO);
        });

        var convenio = SiacPlugin.convenioAtual();

        var linhas = '';
        var quantidade = 0;
        var nrConvenio = convenio.NR_CONVENIO;
        var atualizadoClass = null;
        var atualizadoLabel = null

        for (var i in convenios) {
            if (SiacPlugin.convenioAtualizadoVerificar(convenios[i].VL_DATA_ATUALIZACAO)) {
                atualizadoClass = 'atualizado';
                atualizadoLabel = 'Atualizado';
                quantidade++;
            } else {
                atualizadoClass = 'pendente';
                atualizadoLabel = 'Pendente';
            }

            //pega id do primeiro convênio pendente
            if (parseInt(nrConvenio) === parseInt(convenios[i].NR_CONVENIO)) {
                //adiciona o numero do processo ao convenio atual
                convenio.NR_PROCESSO = $(".field", ".numeroProcesso").html();

                if ($("#menu_link_-1941304600_-1941304600").attr("href").indexOf("mandatarias") !== -1) {
                    convenio.quadro_resumo_link = jQuery("#menu_link_-1941304600_-1941304600").attr("href");

                    //cadastra NR_PROCESSO no convenio atual
                    localStorage.setItem(localStorage.getItem("indexConvenio"), JSON.stringify(convenio));
                }


                atualizadoLabel = 'Atualizando...';
            }


            linhas += `<li>Convênio ${convenios[i].NR_CONVENIO} <label class="${atualizadoClass}">${atualizadoLabel}</label> ${SiacPlugin.timestampTodata(convenios[i].VL_DATA_ATUALIZACAO)}</li>`;
        }

        $('body').after(`
            <div class="siac-box">
                <img src="https://upload.wikimedia.org/wikipedia/pt/thumb/4/43/SUDAM_Logo.png/250px-SUDAM_Logo.png"/>
                <h4>Atualizando o convênio ${nrConvenio} aguarde... ${quantidade} de ${convenios.length}</h4>
                <p>Você será redirecionado para o SIAC quando finalizado</p>
                <div class="siac-box-status">
                    <div class="scroll-box">
                        <ul>
                            ${linhas}
                        <ul>
                    </div>
                </div>
            </div>
        `);

        //espera X segundos para que o usuário consiva visualizar a tela de status antes da página ser redirecionada
        setTimeout(function () {

            //verifica se existe uma cláusula suspensiva
            if ($('input[name=editarDadosPropostaDetalharPropostaDetalharClausulaSuspensivaForm]').length > 0) {
                //redireciona para a clausula suspensiva
                $('input[name=editarDadosPropostaDetalharPropostaDetalharClausulaSuspensivaForm]').trigger("click");
            } else {
                //se não existir enviar para a tela das movimentações
                window.location.href = SiacPlugin.config.MANTER_MOVIMENTACAO;
            }

        }, 3000);
        ;
    },
    cadastraPrestacaoContas: function (dados, callback) {
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=cadastraPrestacaoContasPlugin', dados, function (resource) {
            if (callback) {
                callback(dados);
            }
        }, 'json');
    },
    cadastraQuadroResumo: function (dados, callback) {
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=cadastraQuadroResumoPlugin', dados, function (resource) {
            if (callback) {
                callback(dados);
            }
        }, 'json');
    },
    cadastraSituacaoPbTr: function (dados, callback) {
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=cadastraSituacaoPbTr', dados, function (resource) {
            if (callback) {
                callback(dados);
            }
        }, 'json');
    },
    cadastraMovimentacaoPlugin: function (dados, callback) {
        /**
         * Método para cadastrar as movimentações financeiras
         */
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=cadastraMovimentacaoPlugin', {movimentacoes: dados}, function (resource) {
            if (callback) {
                callback(dados);
            }
        }, 'json');
    },
    cadastraConveioPlugin: function (dados, callback) {
        /**
         * Método cadastrar os dados da cláusula suspensiva
         */
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=cadastraConveioPlugin', dados, function (resource) {
            if (callback) {
                callback(dados);
            }
        }, 'json');
    },
    conveioDataAtualizarPlugin: function (convenio, callback) {
        /**
         * Método para informar que o convenio esta sendo atualizado
         */
        $.post(SiacPlugin.config.URL_API + 'plugin-convenios.php?acao=conveioDataAtualizarPlugin', convenio, function (resource) {
            if (callback) {
                callback();
            }
        }, 'json');
    },
    //### ---- Funções auxiliares ----###//
    dataAtual: function () {

        var dataAtual = new Date();
        var dia = dataAtual.getDate();
        var mes = dataAtual.getMonth() + 1;
        var ano = dataAtual.getYear();
        var hora = dataAtual.getHours();
        var minuto = dataAtual.getMinutes();
        var segundo = dataAtual.getSeconds();

        if (dia < 10) {
            dia = '0' + dia;
        }
        if (mes < 10) {
            mes = '0' + mes;
        }
        if (ano < 1000) {
            ano += 1900;
        }

        return ano + '-' + mes + '-' + dia;
    },
    convenioAtualizadoVerificar: function (data) {
        var dataAtual = new Date();
        var dia = dataAtual.getDate();
        var mes = dataAtual.getMonth() + 1;
        var ano = dataAtual.getYear();
        var hora = dataAtual.getHours();
        var minuto = dataAtual.getMinutes();
        var segundo = dataAtual.getSeconds();

        if (dia < 10) {
            dia = '0' + dia;
        }
        if (mes < 10) {
            mes = '0' + mes;
        }
        if (ano < 1000) {
            ano += 1900;
        }

        if (data && data.indexOf(ano + '-' + mes + '-' + dia) > -1) {
            return true
        } else {
            return false;
        }
    },
    timestampTodata: function (data) {
        if (data) {

            return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);

//            var mydate = new Date(data + "T20:17:53.730Z");
//
//            var dia = mydate.getDate();
//            var mes = mydate.getMonth() + 1;
//            var ano = mydate.getYear();
//
//            if (mes < 10) {
//                mes = '0' + mes;
//            }
//            if (ano < 1000) {
//                ano += 1900
//            }
//
//            return `${dia}/${mes}/${ano}`;
        } else {
            return '';
        }
    }
};